//
//  TFAuth.swift
//  BloomingBuyer
//
//  Created by Waqas Rasheed on 20/03/2021.
//

import OAuthSwift

public struct TFAuth {
    
    private static var oauthswift: OAuth2Swift!
    
    public static func setOauthswift(oauthswift: OAuth2Swift) {
        self.oauthswift = oauthswift
    }
    
//    static var oauthswift = OAuth2Swift(
//        consumerKey: "mobile-app", //meera-dx "mobile-app"        // [1] Enter google app settings
//        consumerSecret:"ZXhhbXBsZS1hcHAtc2VjcmV1",        // No secret required
//        authorizeUrl: Config.ssoURL + "/auth",
//        accessTokenUrl: Config.ssoURL + "/token",
//        responseType: "code"
//    )
    
//    static func resetSSOurl() {
//        oauthswift = OAuth2Swift(
//            consumerKey: "mobile-app",
//            consumerSecret:"ZXhhbXBsZS1hcHAtc2VjcmV1",
//            authorizeUrl: Config.ssoURL + "/auth",
//            accessTokenUrl: Config.ssoURL + "/token",
//            responseType: "code"
//        )
//    }
    
    public static func authenticate(with userName: String, password: String, completion: @escaping (TFTokenModel?,OAuthSwiftError?) -> Void) {
        
        TFAuth.preparingOth2WithPassword(with: userName, password: password) { (dic) in
            
            var token_id: String?
            var token_type: String?
            var expireIn: Double?
            
            if let acToken = dic["access_token"] as? String, let refToken = dic["refresh_token"] as? String {
                
                
                if let idtoken = dic["id_token"] as? String {
                    token_id = idtoken
                }
                
                if let tokentype = dic["token_type"] as? String {
                    token_type = tokentype
                }
                
                if let expiresin = dic["expires_in"] as? Double {
                    expireIn = expiresin
                }
                
                let tokeModel = TFTokenModel(access_token: acToken, refresh_token: refToken, id_token: token_id, token_type: token_type, expires_in: expireIn)
                completion(tokeModel,nil)
            }
            
            
        } failure: { (error) in
            
            print(error.localizedDescription)
            completion(nil, error)
        }
    }
    
    public static func preparingOth2WithPassword(with userName:String, password: String, jsonHandler:@escaping(_ json: [String:Any]) -> Void, failure:@escaping(_ error: OAuthSwiftError)-> Void)
    {
        guard oauthswift != nil else {
            precondition(oauthswift != nil, "oauthswift is nil, please set it")
            return
        }
        oauthswift.authorize(username: userName, password: password, scope: "openid email groups profile offline_access") { (result) in
            switch(result)
            {
            
            case .success(let (_,response ,_ )):
                if let jsonResponce = response?.data
                {
                    do {
                        let jsonResult = try JSONSerialization.jsonObject(with: jsonResponce, options: JSONSerialization.ReadingOptions.allowFragments)
                        
                        let parseJson = jsonResult as! [String:Any]
                        jsonHandler(parseJson)
                    }
                    catch {
                        let notValid = OAuthSwiftError.configurationError(message: "json not valid")
                        failure(notValid)
                    }
                }
                else{
                    let notValid = OAuthSwiftError.configurationError(message: "No user found")
                    failure(notValid)
                }
                
            case.failure(let error):
                
                let errorDic = error.errorUserInfo;
                
                if(self.outhErrorHandler(dic: errorDic).code == 401)
                {
                    let notValid = OAuthSwiftError.configurationError(message: "Invalid email or password.")
                    failure(notValid)
                }
                else if(self.outhErrorHandler(dic: errorDic).code == 400)
                {
                    failure(error)
                }
                else
                {
                    let meessages = self.outhErrorHandler(dic: errorDic)
                    let message = meessages.errorStr + "\(meessages.code)"
                    let notValid = OAuthSwiftError.configurationError(message: message)
                    failure(notValid)
                }
            }
        }
        
    }
    
    private static func outhErrorHandler(dic: [String:Any]?) -> (code:Int,errorStr:String)
    {
        if let _ = dic
        {
            if let m = dic!["error"] as? NSError
            {
                print(m.localizedDescription)
                print(m.code);
                return (code:m.code,errorStr:m.localizedDescription)
            }
        }
        
        return (code:0,errorStr:"Unknown error");
    }
}

extension TFAuth {
    
    public static func renewAcessToken(refreshToken:String,_ completionHandler: @escaping (_ json: [String:Any]?) -> Void) {
        guard NetworkHelper.sharedInstance.isValidText(refreshToken) == true else {
            completionHandler(nil)
            return
        }
        
        oauthswift.renewAccessToken(withRefreshToken: refreshToken) { (result) in
            switch(result)
            {
            case.success(let(_, _, oauthParams)):
                
                if let _ = oauthParams["access_token"] as? String,
                   let _ = oauthParams["refresh_token"] as? String,
                   let _ = oauthParams["expires_in"] as? Double {
                    
                    completionHandler(oauthParams)
                } else {
                    completionHandler(nil)
                }
                break;
            case .failure(let error):
                
                print("renewAuthToken error: \(error.localizedDescription)")
                completionHandler(nil)
                break;
            }
        }
    }
    
    public static func renewToken(refreshToken:String,_ completionHandler: @escaping (TFTokenModel?) -> Void) {
        guard NetworkHelper.sharedInstance.isValidText(refreshToken) == true else {
            completionHandler(nil)
            return
        }
        
        TFAuth.renewAcessToken(refreshToken: refreshToken) { (dic) in
            
            if let dic = dic {
                
                var token_id: String?
                var token_type: String?
                var expireIn: Double?
                
                if let acToken = dic["access_token"] as? String, let refToken = dic["refresh_token"] as? String {
                    
                    
                    if let idtoken = dic["id_token"] as? String {
                        token_id = idtoken
                    }
                    
                    if let tokentype = dic["token_type"] as? String {
                        token_type = tokentype
                    }
                    
                    if let expiresin = dic["expires_in"] as? Double {
                        expireIn = expiresin
                    }
                    
                    let tokeModel = TFTokenModel(access_token: acToken, refresh_token: refToken, id_token: token_id, token_type: token_type, expires_in: expireIn)
                    
                    completionHandler(tokeModel)
                }
            }
            else {
                completionHandler(nil)
            }
        }
    }
}


extension TFAuth {
    public struct TFTokenModel {
        public let access_token: String
        public let refresh_token: String
        public let id_token: String?
        public let token_type: String?
        public let expires_in: Double?
        
        /*public mutating func changeAccessToken() {
            self.access_token = "eys"
        }
        
        public mutating func changeRefreshToken() {
            self.refresh_token = "eys"
        }*/
        
        public func printValues() {
            print("access_token:\(access_token)")
            print("refresh_token:\(refresh_token)")
            print("id_token:\(id_token ?? "")")
            print("token_type:\(token_type ?? "")")
            print("expires_in:\(expires_in ?? -1)")
        }
    }
}
