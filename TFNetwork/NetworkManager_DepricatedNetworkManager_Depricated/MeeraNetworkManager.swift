//
//  QaisNetworkManager.swift
//  QaisApp
//
//  Created by Muhammad Arslan Khalid on 03/11/2018.
//  Copyright © 2018 Target. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import OAuthSwift
import MobileCoreServices

public let Network = MeeraNetworkManager.sharedInstance

public class MeeraNetworkManager: NSObject {
    
    static let sharedInstance = MeeraNetworkManager()
    
    //MARK:- For GraphQl Api's
    public func asyncCallApiForJSONResult(WithBaseUrl baseUrl:String,
                                   WithQueryParameter queryParameter:[String:Any]?,
                                   WithDelegate delegate:UIViewController?,
                                   completion:@escaping serviceCompletionerHandler)
    {
        
        let apiReq = TFRequest(baseUrl: baseUrl, method: .post, bodyPerameter: queryParameter)
        
        TFNetwork.asyncCall(apiRequest: apiReq, requiredResponce: .json,WithDelegate: delegate) { (tfResponse) in
            
            if tfResponse.error == nil {
                if let dt = tfResponse.data {
                    let jsonVal = JSON(dt)
                    completion(jsonVal,nil)
                }
                else {
                    completion(nil,ErrorModel(errorTitle: "", errorDescp: "data not found", error: nil))
                }
            }
            else {
                completion(nil,tfResponse.error)
            }
        }
    }
    
    //MARK:- For RestFull Api's
    public func asyncCallRestFullApi(baseUrl:String!,
                              pathUrl:String ,
                              parameter:[String:Any]?,
                              serviceMethod:ServiceType,
                              success:@escaping(_ json:JSON) -> Void,
                              failure:@escaping (_ errorMode:ErrorModel) -> Void)
    {
        
        var headers: HTTPHeaders?
        
        if (pathUrl.contains("register") && !pathUrl.contains("device-token-manager/register") && !pathUrl.contains("device-token-manager/unregister")) || pathUrl.contains("verify-user") || pathUrl.contains("send-verified-code") || pathUrl.contains("check-verified-code") || pathUrl.contains("forgot-password") || pathUrl.contains("verify-user")
        {
            headers = TFRequest.getSSOHeader()
        }
        else
        {
            headers = TFRequest.getAuthHeader(authToken: "")
        }
        
        let httpMethod = HTTPMethod(rawValue: serviceMethod.rawValue)
        let apiReq = TFRequest(baseUrl: baseUrl, path: pathUrl, method: httpMethod, bodyPerameter: parameter, header: headers)
        
        TFNetwork.asyncCall(apiRequest: apiReq, requiredResponce: .json) { (tfResponse) in
            
            if tfResponse.error == nil {
                if let dt = tfResponse.data {
                    let jsonVal = JSON(dt)
                    success(jsonVal)
                }
                else {
                    failure(ErrorModel(errorTitle: "", errorDescp: "data not found", error: nil))
                }
            }
            else {
                
                failure(tfResponse.error ?? ErrorModel())
            }
            
        }
        
    }
    
    public func asyncCallGeneralApi(baseUrl:String!, pathUrl:String, queryParamters:[String:String]? = nil ,parameter:[String:Any]?,serviceMethod:ServiceType, success:@escaping(_ json:JSON) -> Void, failure:@escaping (_ errorMode:ErrorModel) -> Void)
    {
        let httpMethod = HTTPMethod(rawValue: serviceMethod.rawValue)
        let header = TFRequest.contentTypeHeader()
        
        let apiReq = TFRequest(baseUrl: baseUrl, path: pathUrl, method: httpMethod, quertyString: queryParamters, header: header)
        
        TFNetwork.asyncCall(apiRequest: apiReq, requiredResponce: .json) { (tfResponse) in
            
            if tfResponse.error == nil {
                if let dt = tfResponse.data {
                    
                    let jsonVal = JSON(dt)
                    
                    DispatchQueue.main.async {
                        success(jsonVal)
                    }
                }
                else {
                    failure(ErrorModel(errorTitle: "", errorDescp: "data not found", error: nil))
                }
            }
            else {
                failure(tfResponse.error ?? ErrorModel())
            }
        }
        
    }
    
    func asyncCallApiForJSONResultJob(WithBaseUrl baseUrl:String,WithQueryParameter queryParameter:Any?,WithServiceMethod serviceMethod:ServiceType,WithResponce responceType:ResponceType,WithDelegate delegate:UIViewController?,completion:@escaping jobServiceCompletionerHandler)
    {
        
        if(serviceMethod == .POST || serviceMethod == .PUT || serviceMethod == .DELETE )
        {
            if let parameters = queryParameter as? [String:Any]
            {
                let httpMethod = HTTPMethod(rawValue: serviceMethod.rawValue)
                
                var apiReq: TFRequest!
                if parameters.keys.contains("arrayObject") == false {
                    apiReq = TFRequest(baseUrl: baseUrl, method: httpMethod, bodyPerameter: parameters)
                }
                else {
                    
                    apiReq = TFRequest(baseUrl: baseUrl, method: httpMethod, bodyPerameter: parameters["arrayObject"])
                    
                }
                
                // if url containt the access_token in a queryString.
                // This method will remove it as query string, and add it in the TFRequest object parameter quertyString
                // This method use for old networkmanger API
                let newTfReq = apiReq.fixQueryStringRequest()
                
                TFNetwork.asyncCall(apiRequest: newTfReq, requiredResponce: .json) { (tfResponse) in
                    
                    if tfResponse.error == nil {
                        if let dt = tfResponse.data {
                            let jsonVal = JSON(dt)
                            completion(jsonVal,nil, nil)
                        }
                        else {
                            completion(nil,"", "data not found")
                        }
                    }
                    else {
                        
                        completion(nil,"", tfResponse.error?.localizedDescription)
                    }
                    
                }
            }
            else if let _ = queryParameter as? String
            {
                fatalError("cannot handle this type of perameter")
            }
        }
    }
}


//extension MeeraNetworkManager {
//
//    //MARK:- File Upload Methods
//    func asynchronousWorkWithFile(_ baseUrlStr:String,_ serviceType:ServiceType,_ parameters:[String:Any]?,_ arrImages:[AttachmentUploadModel]?,_ delegate:UIViewController?,withCompletionhandler completion:@escaping serviceCompletionerHandler) -> Void
//    {
//        //bucket=upload&share_with=sys:anonymous&permission=view addprodcut
//        //bucket=qais&share_with=sys:anonymous
//
//        let imageBaseUrlUpload = baseUrlStr + "/upload?bucket=upload&share_with=sys:anonymous,sys:authenticated&permission=view"
//
//
//        //if domainName == "demo-aws.meeraspace.com" || domainName == "om.xeena.com" || domainName == "xeena.com" {
//        // imageBaseUrlUpload = baseUrlStr + #"/upload?bucket=zeena&share_with=sys:anonymous,sys:authenticated&meta={"fm":{"group":"target-zeena-file-group","source":"zeena"}}"#
//        //}
//
//        let mainUrl:URL = URL(string: imageBaseUrlUpload.encodeUrl!)!
//        var request = URLRequest(url: mainUrl)
//        request.httpMethod = serviceType.rawValue
//        let boundary  = "---------------------------147378098314876653456641449"
//        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
//        request.setValue("Bearer \(UserInfo.userAccessToken.accessToken)", forHTTPHeaderField: "Authorization")
//
//
//        var body = NSMutableData();
//        if let parameters = parameters
//        {
//            for (key, value) in parameters {
//
//                body.appendString(string: "--\(boundary)\r\n")
//                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
//                body.appendString(string: "\(value)\r\n")
//            }
//
//        }
//
//        self.addImageFileForUploading(body: &body,arrImages: arrImages!, boundary,"profilePic",true);
//        body.appendString(string: "--\(boundary)--\r\n")
//
//        request.httpBody = body as Data
//        request.setValue(String(body.length), forHTTPHeaderField: "Content-Length")
//        let session  = URLSession(configuration: URLSessionConfiguration.default)
//        //    if AppUtility.hasValidText(UserInfo.userAccessToken.accessToken)
//        //    {
//        //        request.setValue("Bearer \(UserInfo.userAccessToken.accessToken)", forHTTPHeaderField: "Authorization")
//        //    }
//
//
//        let task =  session.dataTask(with: request) { data, response, error in
//
//            DispatchQueue.main.async {
//
//
//                if let _  = error
//                {
//                    completion(nil,ErrorModel(errorTitle: "Xeena", errorDescp: (error! as NSError).debugDescription, error: nil))
//
//                    return
//                }
//
//                // let httpResponse = response as? HTTPURLResponse
//                guard let _ = data else{
//
//                    completion(nil,ErrorModel(errorTitle: "Xeena", errorDescp: "Got Response Nill", error: nil))
//
//
//                    return
//                }
//
//
//
//
//
//                //                let strData = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
//
//
//                //                print("Body: \(String(describing: strData))")
//
//                do
//                {
//                    let json = try JSON(data: data!)
//                    //                    print(json.rawValue)
//                    let parseJson = json.rawValue as! [String:Any]
//                    let graphQLError = parseJson["errors"] as? [[String:Any]]
//
//
//
//                    if  let _ = graphQLError
//                    {
//                        if (graphQLError!.count > 0)
//                        {
//                            //                            print(graphQLError![0])
//                            let msgDic = graphQLError![0];
//                            let msg = msgDic["message"] as? String;
//
//                            //KadirUTIL.sharedInstance.displayAlert(withTitle: "GraphQL Error", andMessage: graphQLError![0], delegate);
//
//                            completion(nil,ErrorModel(errorTitle: "Xeena", errorDescp: msg ?? "Unknown Error", error: nil))
//
//
//                            return;
//                        }
//                    }
//
//
//
//
//                    if let _ = json.rawValue as? [String:Any]
//                    {
//                        completion(json, nil)
//                    }
//                    else
//                    {
//                        completion(nil,ErrorModel(errorTitle: "Json Parsin Error", errorDescp: "Json parsing issue", error: nil))
//
//                    }
//
//                }
//                catch
//                {
//                    completion(nil,ErrorModel(errorTitle: "Json Parsin Error", errorDescp: "Json parsing issue", error: nil))
//                }
//
//            }
//
//
//
//        }
//
//        task.resume()
//
//    }
//
//    func addImageFileForUploading( body:inout NSMutableData,arrImages:[AttachmentUploadModel],_ boundary:String,_ fileNameKey:String?, _ isNeedCompressImg:Bool)
//    {
//        for val in arrImages {
//            let fileName = val.name!
//
//            guard let imgData = val.data else{continue}
//
//            let mimType =  self.MIMEType(fileExtension: val.name!.pathExtension)
//
//            body.appendString(string: "--\(boundary)\r\n")
//
//            body.appendString(string: "Content-Disposition: form-data;  name=\"image\"; filename=\"\(fileName)\"\r\n")
//            body.appendString(string: "Content-Type: \(String(describing: mimType))\r\n\r\n")
//            body.append(imgData)
//            body.appendString(string: "\r\n")
//        }
//    }
//
//
//
//    private func MIMEType(fileExtension: String) -> String? {
//        if !fileExtension.isEmpty {
//            let UTIRef = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, fileExtension as CFString, nil)
//            let UTI = UTIRef?.takeUnretainedValue()
//            UTIRef?.release()
//
//            let MIMETypeRef = UTTypeCopyPreferredTagWithClass(UTI!, kUTTagClassMIMEType)
//            if MIMETypeRef != nil
//            {
//                let MIMEType = MIMETypeRef?.takeUnretainedValue()
//                MIMETypeRef?.release()
//                return MIMEType as String?
//            }
//        }
//        return nil
//    }
//}
