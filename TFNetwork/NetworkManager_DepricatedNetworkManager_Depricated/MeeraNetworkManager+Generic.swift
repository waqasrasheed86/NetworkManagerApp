//
//  QaisNetworkManager+Generic.swift
//  Meera Work Space
//
//  Created by Muhammad Sajad on 18/03/2019.
//  Copyright © 2019 Target OilField Services. All rights reserved.
//

import Foundation
import Alamofire
import OAuthSwift
import SwiftyJSON

/*class RequestOperation: Operation {
    //private let compositeRequest: DataRequest? = nil
    
    let url: URL
    let method: HTTPMethod
    let parameters: Parameters?
    let encoding: ParameterEncoding
    let headers: HTTPHeaders?
    
    var dataCallBack: ((DataResponse<Data,AFError>) -> Void)?
    var jsonCallBack: ((DataResponse<Any,AFError>) -> Void)?
    
    init(url: URL, method: HTTPMethod, parameters: Parameters?, encoding: ParameterEncoding, headers: HTTPHeaders?) {
        self.url = url
        self.method = method
        self.parameters = parameters
        self.encoding = encoding
        self.headers = headers
    }
    
    /*init(request: DataRequest) {
     self.compositeRequest = request
     }*/
    
    var requestCompleted: Bool = false {
        willSet {
            willChangeValue(forKey: "isFinished")
        }
        didSet {
            didChangeValue(forKey: "isFinished")
        }
    }
    var requestInProgress: Bool = false {
        willSet {
            willChangeValue(forKey: "isExecuting")
        }
        didSet {
            didChangeValue(forKey: "isExecuting")
        }
    }
    
    private let session: Session = {
        return AF
        //        let manager = ServerTrustManager(evaluators: ["https://gregion.fluxble.com": DisabledEvaluator(),"https://api.target.fluxble.com": DisabledEvaluator()])
        //        let configuration = URLSessionConfiguration.af.default
        //        return Session(configuration: configuration, serverTrustManager: manager)
    }()
    
    override func main() {
        if isCancelled {
            return
        }
        requestInProgress = true
        
        var updatedHeaders: HTTPHeaders? = nil
        
        if let headers = headers {
            updatedHeaders = headers
            if let auth = headers["Authorization"],
               auth.hasPrefix("Bearer") {
                updatedHeaders!["Authorization"] = "Bearer \(AuthTokenTests ?? UserInfo.userAccessToken.accessToken)"
            }
        }
        
        
        let request = session.request(url,
                                      method: method,
                                      parameters: parameters,
                                      encoding: encoding,
                                      headers: updatedHeaders).cURLDescription { (curlDesc) in
                                        //print("Curl Request:- \(curlDesc)")
                                      }
        request.responseString { (dataStr) in
            // print(dataStr)
        }
        
        request.responseData { [unowned self] (response) in
            if self.isCancelled {
                return
            }
            self.dataCallBack?(response)
            if !self.requestCompleted {
                self.completeOperation()
            }
            
            let httpResponse = response.response
            
            if response.error != nil || httpResponse?.statusCode != 200  {
                switch response.result {
                
                case .success( _):
                    print("==================ERROR=========================");
                    print("Path: \(String(describing: response.request?.urlRequest))")
                    print("Status Code: \(String(describing: httpResponse?.statusCode))")
                    print("==================END=========================");
                case .failure(let error):
                    
                    print("==================ERROR=========================");
                    print("Error: \(error.errorDescription ?? "") Path: \(String(describing: response.request?.urlRequest))")
                    print("Status Code: \(String(describing: httpResponse?.statusCode))")
                    if(httpResponse?.statusCode != 200) {
                        //String(decoding: response.data!, as: UTF8.self)
                    }
                    //JSON(String(decoding: response.data!, as: UTF8.self))
                    print("==================END=========================");
                    
                }
            }
            
        }.responseJSON { (response) in
            
            if self.isCancelled {
                return
            }
            self.jsonCallBack?(response)
            
            if !self.requestCompleted {
                self.completeOperation()
            }
            
            let httpResponse = response.response
            
            if response.error != nil || httpResponse?.statusCode != 200  {
                switch response.result {
                
                case .success( _):
                    print("==================ERROR=========================");
                    print("Path: \(String(describing: response.request?.urlRequest))")
                    print("Status Code: \(String(describing: httpResponse?.statusCode))")
                    print("==================END=========================");
                case .failure(let error):
                    
                    print("==================ERROR=========================");
                    print("Error: \(error.errorDescription ?? "") Path: \(String(describing: response.request?.urlRequest))")
                    print("Status Code: \(String(describing: httpResponse?.statusCode))")
                    if(httpResponse?.statusCode != 200) {
                        //String(decoding: response.data!, as: UTF8.self)
                    }
                    //JSON(String(decoding: response.data!, as: UTF8.self))
                    print("==================END=========================");
                    
                }
            }
            
        }
    }
    
    override var isFinished: Bool {
        return requestCompleted
    }
    
    override var isExecuting: Bool {
        return requestInProgress
    }
    
    override var isAsynchronous: Bool {
        return true
    }
    
    private func completeOperation() {
        requestInProgress = false
        requestCompleted = true
    }
    
}

class RefreshTokenOperation: Operation {
    private let completionHandler: (Bool) -> Void
    
    init(completionHandler: @escaping (Bool) -> Void) {
        self.completionHandler = completionHandler
    }
    
    var completed: Bool = false {
        willSet {
            willChangeValue(forKey: "isFinished")
        }
        didSet {
            didChangeValue(forKey: "isFinished")
        }
    }
    var inProgress: Bool = false {
        willSet {
            willChangeValue(forKey: "isExecuting")
        }
        didSet {
            didChangeValue(forKey: "isExecuting")
        }
    }
    
    override func main() {
        if isCancelled {
            return
        }
        inProgress = true
        
        TFNetwork.renewAuthToken { (success) in
            if self.isCancelled {
                return
            }
            
            self.completionHandler(success)
            self.completeOperation()
        }
    }
    
    override var isFinished: Bool {
        return completed
    }
    
    override var isExecuting: Bool {
        return inProgress
    }
    
    override var isAsynchronous: Bool {
        return true
    }
    
    private func completeOperation() {
        inProgress = false
        completed = true
    }
    
}

extension MeeraNetworkManager {
    
    private func checkAuthTokenValidity() {
        
        if AppUtility.hasValidText(UserInfo.userId) {
            
            if let expires_in = UserInfo.userAccessToken.expires_in,
               let token_date = UserInfo.userAccessToken.token_date {
                //print("QaisNetworkManager: checkAuthTokenValidity() token time elasped: \(Date().timeIntervalSince(token_date))")
                if Date().timeIntervalSince(token_date) > expires_in,
                   !isTokenRefreshedRecently {
                    requestNewToken()
                }
            }
        }
    }
    
    //Public API
    func requestJSON(
        _ path: String,
        method: HTTPMethod = .get,
        parameters: Parameters? = nil,
        encoding: ParameterEncoding = JSONEncoding.default,
        headers: HTTPHeaders? = nil,
        completionHandler: @escaping (DataResponse<Any,AFError>) -> Void) {
        
        if let url = URL(string: path) {
            requestJSON(url,
                        method: method,
                        parameters: parameters,
                        encoding: encoding,
                        headers: headers,
                        completionHandler: completionHandler)
        }
    }
    
    func requestJSON(
        _ url: URL,
        method: HTTPMethod = .get,
        parameters: Parameters? = nil,
        encoding: ParameterEncoding = JSONEncoding.default,
        headers: HTTPHeaders? = nil,
        completionHandler: @escaping (DataResponse<Any,AFError>) -> Void) {
        
        checkAuthTokenValidity()
        
        let requestOperation = RequestOperation(url: url,
                                                method: method,
                                                parameters: parameters,
                                                encoding: encoding,
                                                headers: headers)
        
        /*let requestOperation =  RequestOperation(request: Alamofire.request(url,
         method: method,
         parameters: parameters,
         encoding: encoding,
         headers: headers))*/
        
        requestOperation.jsonCallBack =  { [weak self] (response) in
            
            guard let strongSelf = self else { return }
            
            if response.response?.statusCode == AuthenticationFailedStatusCode { //User Token has expired
                
                if !strongSelf.isTokenRefreshedRecently {     //Renew token
                    strongSelf.requestNewToken()
                }
                //Re request
                strongSelf.requestJSON(url,
                                       method: method,
                                       parameters: parameters,
                                       encoding: encoding,
                                       headers: headers,
                                       completionHandler: completionHandler)
            } else {
                completionHandler(response)
            }
        }
        startNewReqeustOperation(requestOperation)
    }
    
    
    func requestData(
        _ path: String,
        method: HTTPMethod = .get,
        parameters: Parameters? = nil,
        encoding: ParameterEncoding = JSONEncoding.default,
        headers: HTTPHeaders? = nil,
        completionHandler: @escaping (DataResponse<Data,AFError>) -> Void) {
        
        if let url = URL(string: path) {
            requestData(url,
                        method: method,
                        parameters: parameters,
                        encoding: encoding,
                        headers: headers,
                        completionHandler: completionHandler)
        }
    }
    
    func requestData(
        _ url: URL,
        method: HTTPMethod = .get,
        parameters: Parameters? = nil,
        encoding: ParameterEncoding = JSONEncoding.default,
        headers: HTTPHeaders? = nil,
        completionHandler: @escaping (DataResponse<Data,AFError>) -> Void) {
        
        checkAuthTokenValidity()
        
        let requestOperation = RequestOperation(url: url,
                                                method: method,
                                                parameters: parameters,
                                                encoding: encoding,
                                                headers: headers)
        
        /* let requestOperation = RequestOperation(request: Alamofire.request(url,
         method: method,
         parameters: parameters,
         encoding: encoding,
         headers: headers))*/
        
        requestOperation.dataCallBack =  { [weak self] (response) in
            
            guard let strongSelf = self else { return }
            
            if response.response?.statusCode == AuthenticationFailedStatusCode { //User Token has expired
                
                if !strongSelf.isTokenRefreshedRecently {     //Renew token
                    strongSelf.requestNewToken()
                }
                //Re request
                strongSelf.requestData(url,
                                       method: method,
                                       parameters: parameters,
                                       encoding: encoding,
                                       headers: headers,
                                       completionHandler: completionHandler)
            } else {
                completionHandler(response)
            }
        }
        startNewReqeustOperation(requestOperation)
    }
    
    
    //Private API
    private func requestNewToken() {
        isTokenRefreshedRecently = true
        let refreshTokenOperation = RefreshTokenOperation(completionHandler: { (success) in
            //self.refreshTokenOperation = nil
            if !success {
                self.queue.cancelAllOperations()
                self.goBackToLogin()
            }
            //CallApiManager.shared.resetCredentials()
        })
        queue.addOperation(refreshTokenOperation)
    }
    
    private func startNewReqeustOperation(_ operation: RequestOperation) {
        /*if let refreshTokenOperation = refreshTokenOperation {
         operation.addDependency(refreshTokenOperation)
         }*/
        
        let refreshTokenOp = queue.operations.filter { return $0 is RefreshTokenOperation}
        
        if refreshTokenOp.count == 1 {
            operation.addDependency(refreshTokenOp[0])
        }
        queue.addOperation(operation)
    }
    
    private func goBackToLogin() {
        UserInfo.userId = "";
        //UserInfo.defaultWorkspaceId = nil
        //UserInfo.removeSaveData();
        //AppUtility.arrWorkspaces.removeAll();
        //AppCache.clearAppCache();
        //AppSocket.socketClient.disconnect();
        //CallApiManager.shared.socketManger.disConnect()
        //NoteUTIL.deleteInstanceID()
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        //Server.deleteAll()
        //appDel.windowRootController();
        
    }
    
    /*func parseJsonResponse( response:DataResponse<Any>) -> (JsonResponseEnum)
     {// completion:@escaping serviceCompletionerHandler
     switch response.result
     {
     case .success:
     
     let httpResponse = response.response
     if httpResponse?.statusCode != 200  //!=200
     {
     
     return JsonResponseEnum.Failure(error: ErrorModel(errorTitle: "Server Error", errorDescp: "Unknown Error", error: nil))
     //return (nil, ErrorModel(errorTitle: "Server Error", errorDescp: "Unknown Error", error: nil))
     }
     else
     {
     if let res = response.result.value
     {
     let jsonVal = JSON(res)
     let parseJson = jsonVal.rawValue as! [String:Any]
     let graphQLError = parseJson["errors"] as? [[String:Any]]
     
     
     
     if  let _ = graphQLError
     {
     if (graphQLError!.count > 0)
     {
     print(graphQLError![0])
     let msgDic = graphQLError![0];
     if let msg = msgDic["message"] as? String
     {
     return  JsonResponseEnum.Failure(error: ErrorModel(errorTitle: "GraphQL Error", errorDescp: msg, error: nil, errorCode: 0))
     //return (nil,ErrorModel(errorTitle: "GraphQL Error", errorDescp: msg, error: nil, errorCode: 0))
     }
     else
     {
     return JsonResponseEnum.Failure(error: ErrorModel(errorTitle: "GraphQL Error", errorDescp: "Unknown Error", error: nil, errorCode: 0))
     //return(nil,ErrorModel(errorTitle: "GraphQL Error", errorDescp: "Unknown Error", error: nil, errorCode: 0))
     }
     
     
     }
     }
     
     
     if let _ = jsonVal.rawValue as? [String:Any]
     {
     
     return JsonResponseEnum.success(json: jsonVal)
     //                       // return(jsonVal,nil)
     }
     else
     {
     return JsonResponseEnum.Failure(error: ErrorModel(errorTitle: "Json Parsin Error", errorDescp: "Json parsing issue", error: nil, errorCode:0))
     // return(nil,ErrorModel(errorTitle: "Json Parsin Error", errorDescp: "Json parsing issue", error: nil, errorCode:0))
     
     }
     
     }
     else
     {
     return JsonResponseEnum.Failure(error: ErrorModel(errorTitle: "Server Error", errorDescp: "Got Response Nil", error: nil))
     //return(nil,ErrorModel(errorTitle: "Server Error", errorDescp: "Got Response Nil", error: nil))
     }
     }
     
     
     
     break;
     case .failure(let error):
     return JsonResponseEnum.Failure(error: ErrorModel(errorTitle: "Server Error", errorDescp: error.localizedDescription, error: error))
     //return(nil,ErrorModel(errorTitle: "Server Error", errorDescp: error.localizedDescription, error: error))
     break;
     }
     }*/
}

enum JsonResponseEnum {
    case success(json:JSON)
    case Failure(error:ErrorModel)
}
*/
