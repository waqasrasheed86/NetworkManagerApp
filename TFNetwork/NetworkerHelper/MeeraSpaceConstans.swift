//
//  MeeraSpaceConstans.swift
//  Meera Work Space
//
//  Created by Muhammad Arslan Khalid on 18/11/2018.
//  Copyright © 2018 Target OilField Services. All rights reserved.
//

import Foundation
import SwiftyJSON
import OAuthSwift

//let isDevEnv = true
//var grpcMeeraFSURL = ""
//
//let registerWithInviteCode = true

class NetworkHelper: NSObject {
    @objc static let sharedInstance = NetworkHelper()
    
    func isValidText(_ text:String?) -> Bool
    {
        if let data = text
        {
            if data == "nil"
            {
                return false
            }
            let str = data.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            if str.count>0
            {
                return true
            }
            else
            {
                return false
            }
        }
        else
        {
            return false
        }
        
    }
}
