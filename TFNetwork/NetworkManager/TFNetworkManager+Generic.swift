//
//  TFNetworkManager+Generic.swift
//  BloomingBuyer
//
//  Created by Waqas Rasheed on 20/03/2021.
//

import Alamofire
import OAuthSwift
import SwiftyJSON


class TFRequestOperation: Operation {
    
    let apiRequest: TFRequest
    let encoding: ParameterEncoding
    let header: HTTPHeaders?
    var dataCallBack: ((DataResponse<Data,AFError>) -> Void)?
    var jsonCallBack: ((DataResponse<Any,AFError>) -> Void)?
    var plainTextCallBack: ((DataResponse<String,AFError>) -> Void)?
    var responseType: ResponseType = .json
    
    init(apiRequest: TFRequest, responseType :ResponseType, encoding: ParameterEncoding, header: HTTPHeaders?) {
        
        self.apiRequest = apiRequest
        self.encoding = encoding
        self.header = header
        self.responseType = responseType
    }
    
    private let session: Session = {
        let configuration = URLSessionConfiguration.af.default
        configuration.timeoutIntervalForRequest = 30;
        configuration.waitsForConnectivity = false
        return Session(configuration: configuration)
    }()
    
    var requestCompleted: Bool = false {
        willSet {
            willChangeValue(forKey: "isFinished")
        }
        didSet {
            didChangeValue(forKey: "isFinished")
        }
    }
    var requestInProgress: Bool = false {
        willSet {
            willChangeValue(forKey: "isExecuting")
        }
        didSet {
            didChangeValue(forKey: "isExecuting")
        }
    }
    
    override var isFinished: Bool {
        return requestCompleted
    }
    
    override var isExecuting: Bool {
        return requestInProgress
    }
    
    override var isAsynchronous: Bool {
        return true
    }
    
    private func completeOperation() {
        requestInProgress = false
        requestCompleted = true
    }
}

extension TFRequestOperation {
    
    override func main() {
        if isCancelled {
            return
        }
        requestInProgress = true
        
        let updatedHeaders: HTTPHeaders? = header
        
        guard let url = apiRequest.url else {
            print("url is nil")
            return
        }
        
        let param = apiRequest.bodyPerameter
        let method = apiRequest.method
        
        var request: DataRequest!
        
        if let req = getURlRequest() {
            
            //setting the httpbody if Root object is Array in JSON payload.
            //   AND
            //setting the httpbody if Root object is String
            
            request = session.request(req)
        }
        else {
            //setting the httpbody if Root object is Hashable in JSON payload.
            request = session.request(url,
                                      method: method,
                                      parameters: param as? [String:Any],
                                      encoding: encoding,
                                      headers: updatedHeaders).cURLDescription { (curlDesc) in
                                        //print("Curl Request:- \(curlDesc)")
                                      }
            
        }
        
        if responseType == .plain {
            request.responseString { (dataStr) in
                // print(dataStr)
                if self.isCancelled {
                    return
                }
                
                self.plainTextCallBack?(dataStr)
                if !self.requestCompleted {
                    self.completeOperation()
                }
            }
        }
        else {
            
            request.responseData { [unowned self] (response) in
                if self.isCancelled {
                    return
                }
                self.dataCallBack?(response)
                if !self.requestCompleted {
                    self.completeOperation()
                }
                
                let httpResponse = response.response
                
                if ((response.error != nil || httpResponse?.statusCode != 200) && responseType == .data) {
                    switch response.result {
                    
                    case .success( _):
                        print("==================ERROR=========================");
                        print("Path: \(String(describing: response.request?.urlRequest))")
                        print("Status Code: \(String(describing: httpResponse?.statusCode))")
                        print("==================END=========================");
                    case .failure(let error):
                        
                        print("==================ERROR=========================");
                        print("Error: \(error.errorDescription ?? "") Path: \(String(describing: response.request?.urlRequest))")
                        print("Status Code: \(String(describing: httpResponse?.statusCode))")
                        if(httpResponse?.statusCode != 200) {
                            //String(decoding: response.data!, as: UTF8.self)
                        }
                        //JSON(String(decoding: response.data!, as: UTF8.self))
                        print("==================END=========================");
                        
                    }
                }
                else {
                    /*print("==================Response=========================");
                     
                     print("Path: \(String(describing: response.request?.urlRequest))")
                     print("taskInterval: \(response.metrics?.taskInterval.duration ?? 0)")
                     print("Status Code: \(String(describing: httpResponse?.statusCode))")
                     print("==================END=========================");*/
                }
                
            }
            
            .responseJSON { (response) in
                
                if self.isCancelled {
                    return
                }
                
                self.jsonCallBack?(response)
                
                if !self.requestCompleted {
                    self.completeOperation()
                }
                
                let httpResponse = response.response
                
                if ((response.error != nil || httpResponse?.statusCode != 200) && self.responseType == .json) {
                    switch response.result {
                    
                    case .success( _):
                        print("==================ERROR=========================");
                        print("Path: \(String(describing: response.request?.urlRequest))")
                        print("Status Code: \(String(describing: httpResponse?.statusCode))")
                        print("==================END=========================");
                    case .failure(let error):
                        
                        print("==================ERROR=========================");
                        print("Error: \(error.errorDescription ?? "") Path: \(String(describing: response.request?.urlRequest))")
                        print("Status Code: \(String(describing: httpResponse?.statusCode))")
                        if(httpResponse?.statusCode != 200) {
                            //String(decoding: response.data!, as: UTF8.self)
                        }
                        //JSON(String(decoding: response.data!, as: UTF8.self))
                        print("==================END=========================");
                        
                    }
                }
                else {
                    /*print("==================Response=========================");
                     print("Path: \(String(describing: response.request?.urlRequest))")
                     print("taskInterval: \(response.metrics?.taskInterval.duration ?? 0)")
                     print("Status Code: \(String(describing: httpResponse?.statusCode))")
                     print("==================END=========================");*/
                }
                
            }
        }
        
        
    }
    
}

//MARK:- Refresh Token Opertaion
class TFRefreshTokenOperation: Operation {
    
    private let completionHandler: (Bool) -> Void
    
    init(completionHandler: @escaping (Bool) -> Void) {
        self.completionHandler = completionHandler
    }
    
    var completed: Bool = false {
        willSet {
            willChangeValue(forKey: "isFinished")
        }
        didSet {
            didChangeValue(forKey: "isFinished")
        }
    }
    var inProgress: Bool = false {
        willSet {
            willChangeValue(forKey: "isExecuting")
        }
        didSet {
            didChangeValue(forKey: "isExecuting")
        }
    }
    
    override var isFinished: Bool {
        return completed
    }
    
    override var isExecuting: Bool {
        return inProgress
    }
    
    override var isAsynchronous: Bool {
        return true
    }
    
    private func completeOperation() {
        inProgress = false
        completed = true
    }
    
    
    override func main() {
        if isCancelled {
            return
        }
        
        inProgress = true
        
        TFNetwork.renewAuthToken { (success) in
            if self.isCancelled {
                return
            }
            
            self.completionHandler(success)
            self.completeOperation()
        }
    }
}

extension TFNetworkManager {
    
    private func checkAuthTokenValidity() {
        
        /*if NetworkHelper.sharedInstance.isValidText(self.token?.access_token) {
            
            if let expires_in = UserInfo.userAccessToken.expires_in,
               let token_date = UserInfo.userAccessToken.token_date {
                //print("QaisNetworkManager: checkAuthTokenValidity() token time elasped: \(Date().timeIntervalSince(token_date))")
                if Date().timeIntervalSince(token_date) > expires_in,
                   !isTokenRefreshedRecently {
                    requestNewToken()
                }
            }
        }*/
    }
    
    
    //Public API
    func requestExecute(apiRequest: TFRequest,requiredResponce: ResponseType,
                        encoding: ParameterEncoding = JSONEncoding.default, header: HTTPHeaders?,
                        _ completionHandler: @escaping apiCallBack) {
        
        if requiredResponce == .json {
            requestJSON(apiRequest: apiRequest, responseType: requiredResponce, encoding: encoding, header: header) { (response) in
                completionHandler(response,nil,nil)
            }
        }
        else if requiredResponce == .data {
            
            requestData(apiRequest: apiRequest, responseType: requiredResponce, encoding: encoding, header: header) { (response) in
                completionHandler(nil,response,nil)
            }
        }
        else if requiredResponce == .plain {
            
            requestString(apiRequest: apiRequest, responseType: requiredResponce, encoding: encoding, header: header) { (response) in
                completionHandler(nil,nil,response)
            }
        }
    }
    
    private func requestJSON(apiRequest: TFRequest, responseType: ResponseType,
                             encoding: ParameterEncoding = JSONEncoding.default, header: HTTPHeaders?,
                             completionHandler: @escaping (DataResponse<Any,AFError>) -> Void) {
        
        if let _ = apiRequest.url { //is valid url
            
            checkAuthTokenValidity()
            
            let requestOperation = TFRequestOperation(apiRequest: apiRequest,responseType: responseType, encoding: encoding,header: header)
            
            requestOperation.jsonCallBack =  { [weak self] (response) in
                
                guard let strongSelf = self else { return }
                
                if response.response?.statusCode == AuthenticationFailedStatusCode { //User Token has expired
                    
                    if !strongSelf.isTokenRefreshedRecently {     //Renew token
                        strongSelf.requestNewToken()
                    }
                    
                    //Re request
                    let updatedReq = strongSelf.updateRequestHeaderToken(tfReq: apiRequest, header: header)
                    strongSelf.requestJSON(apiRequest: updatedReq.tfReq,responseType: responseType, encoding: encoding, header: updatedReq.header, completionHandler: completionHandler)
                    
                } else {
                    completionHandler(response)
                }
            }
            
            startTFReqeustOperation(requestOperation)
        }
    }
    
    
    private func requestData(apiRequest: TFRequest, responseType: ResponseType,
                             encoding: ParameterEncoding = JSONEncoding.default, header: HTTPHeaders?,
                             completionHandler: @escaping (DataResponse<Data,AFError>) -> Void) {
        
        if let _ = apiRequest.url { //is valid url
            
            checkAuthTokenValidity()
            
            let requestOperation = TFRequestOperation(apiRequest: apiRequest,responseType: responseType, encoding: encoding,header: header)
            
            requestOperation.dataCallBack =  { [weak self] (response) in
                
                guard let strongSelf = self else { return }
                
                if response.response?.statusCode == AuthenticationFailedStatusCode { //User Token has expired
                    
                    if !strongSelf.isTokenRefreshedRecently {     //Renew token
                        strongSelf.requestNewToken()
                    }
                    
                    //Re request
                    let updatedReq = strongSelf.updateRequestHeaderToken(tfReq: apiRequest, header: header)
                    strongSelf.requestData(apiRequest: updatedReq.tfReq,responseType: responseType, encoding: encoding, header: updatedReq.header, completionHandler: completionHandler)
                    
                } else {
                    completionHandler(response)
                }
            }
            
            startTFReqeustOperation(requestOperation)
        }
    }
    
    private func requestString(apiRequest: TFRequest, responseType: ResponseType,
                               encoding: ParameterEncoding = JSONEncoding.default, header: HTTPHeaders?,
                               completionHandler: @escaping (DataResponse<String,AFError>) -> Void) {
        
        if let _ = apiRequest.url { //is valid url
            
            checkAuthTokenValidity()
            
            let requestOperation = TFRequestOperation(apiRequest: apiRequest,responseType: responseType, encoding: encoding,header: header)
            
            requestOperation.plainTextCallBack =  { [weak self] (response) in
                
                guard let strongSelf = self else { return }
                
                if response.response?.statusCode == AuthenticationFailedStatusCode { //User Token has expired
                    
                    if !strongSelf.isTokenRefreshedRecently {     //Renew token
                        strongSelf.requestNewToken()
                    }
                    
                    //Re request
                    let updatedReq = strongSelf.updateRequestHeaderToken(tfReq: apiRequest, header: header)
                    strongSelf.requestString(apiRequest: updatedReq.tfReq,responseType: responseType,encoding: encoding,header: updatedReq.header, completionHandler: completionHandler)
                    
                } else {
                    completionHandler(response)
                }
            }
            
            startTFReqeustOperation(requestOperation)
        }
    }
    
    //Private API
    private func requestNewToken() {
        
        isTokenRefreshedRecently = true
        let refreshTokenOperation = TFRefreshTokenOperation(completionHandler: { [weak self] (success) in
            
            guard let strongSelf = self else {return}
            if !success {
                strongSelf.queue.cancelAllOperations()
                strongSelf.goBackToLogin()
            }
            else {
                
                DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 60) {
                    strongSelf.isTokenRefreshedRecently = false
                }
                print("Renew Token Succcessfully")
            }
        })
        queue.addOperation(refreshTokenOperation)
    }
    
    private func startTFReqeustOperation(_ operation: TFRequestOperation) {
        /*if let refreshTokenOperation = refreshTokenOperation {
         operation.addDependency(refreshTokenOperation)
         }*/
        
        let refreshTokenOp = queue.operations.filter { return $0 is TFRefreshTokenOperation}
        
        if refreshTokenOp.count == 1 {
            operation.addDependency(refreshTokenOp[0])
        }
        queue.addOperation(operation)
    }
    
    private func goBackToLogin() {
        
        DispatchQueue.main.async {
            //AppUtility.resetDataAll()
            //AppUtility.showRootController()
        }
    }
}


extension TFRequestOperation {
    
    func getURlRequest() -> URLRequest? {
        
        let param = apiRequest.bodyPerameter
        
        guard (param != nil && (param as? [String:Any]) == nil) else {
            return nil
        }
        
        var req = URLRequest(url: apiRequest.url!)
        
        req.httpMethod = apiRequest.method.rawValue
        
        if let h = header {
            req.headers = h
        }
        
        if let s = param as? [Any] {
            
            if JSONSerialization.isValidJSONObject(s) {
                do {
                    req.httpBody = try JSONSerialization.data(withJSONObject: s)
                }
                catch(let error) {
                    print(error.localizedDescription)
                }
            }
        }
        else {
            //set string in body
            if let s = param as? String {
                
                if let newData = s.data(using: .utf8,allowLossyConversion: true) {
                    req.httpBody = newData
                }
            }
        }
        
        return req
    }
}

extension TFNetworkManager {
    
    private func updateRequestHeaderToken(tfReq: TFRequest, header: HTTPHeaders?) -> (tfReq: TFRequest, header: HTTPHeaders?) {
        // when app get new token with the refresh token
        var updatedHeaders: HTTPHeaders? = nil
        
        if let h = header {
            updatedHeaders = h
            if let auth = h["Authorization"],
               auth.hasPrefix("Bearer") {
                //updatedHeaders!["Authorization"] = "Bearer \(AuthTokenTests ?? UserInfo.userAccessToken.accessToken)"
                updatedHeaders!["Authorization"] = "Bearer \(TFNetwork.token?.access_token ?? "")"
            }
        }
        
        guard var _ = tfReq.url else {
            return (tfReq,updatedHeaders)
        }
        
        if let queryPram = tfReq.quertyString {
            
            if let _ = queryPram["access_token"] {
                print("token found in quertyString")
                var prevQueryString = queryPram
                
                //prevQueryString["access_token"] = AuthTokenTests ?? UserInfo.userAccessToken.accessToken
                prevQueryString["access_token"] = TFNetwork.token?.access_token ?? ""
                
                var newTfReq = tfReq
                newTfReq.setQueryString(value: prevQueryString)
                
                return (newTfReq,updatedHeaders)
            }
        }
        
        return (tfReq,updatedHeaders)
        
    }
}

/*var updatedHeaders: HTTPHeaders? = nil
 
 if let h = header {
 updatedHeaders = h
 if let auth = h["Authorization"],
 auth.hasPrefix("Bearer") {
 updatedHeaders!["Authorization"] = "Bearer \(AuthTokenTests ?? UserInfo.userAccessToken.accessToken)"
 print("updated the header token with new token")
 }
 }
 
 let queryItems = URLComponents(string: url.absoluteString)?.queryItems
 if let param1 = queryItems?.filter({$0.name == "access_token"}).first {
 print(param1.value ?? "")
 let cUrl = url.absoluteString.replacingOccurrences(of: param1.value ?? "", with: AuthTokenTests ?? UserInfo.userAccessToken.accessToken)
 url = URL(string: cUrl)!
 }*/
