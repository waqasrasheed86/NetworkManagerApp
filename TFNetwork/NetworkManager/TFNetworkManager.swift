//
//  TFNetworkManager.swift
//  BloomingBuyer
//
//  Created by Waqas Rasheed on 20/03/2021.
//

import UIKit
import SwiftyJSON
import Alamofire
import OAuthSwift

public let TFNetwork = TFNetworkManager.sharedInstance

let AuthenticationFailedStatusCode = 401

public class TFNetworkManager: NSObject {
    
    static let sharedInstance = TFNetworkManager()
    
    var queue = OperationQueue()
    var isTokenRefreshedRecently = false
    var token: TFAuth.TFTokenModel?
    
    public weak var delegate: TFNetworkManagerDelegate?
    
    private var authToken: String? {
        return token?.access_token
    }
    
    var authHeader: HTTPHeaders {
        if authToken?.count ?? 0 > 0 {
            return TFRequest.getAuthHeader(authToken: authToken!)!
        }
        return HTTPHeaders()
    }
    
    public func setToken(authToken: TFAuth.TFTokenModel) {
        self.token = authToken
    }
    
    override init() { }

    public init(authToken: TFAuth.TFTokenModel) {
        super.init()
        self.token = authToken
    }
    
    public init(access_token: String, refresh_token: String, id_token: String, token_type: String, expires_in: Double?) {
        super.init()
        self.token = TFAuth.TFTokenModel(access_token: access_token, refresh_token: refresh_token, id_token: id_token, token_type: token_type, expires_in: expires_in)
    }
    
    
    
}

extension TFNetworkManager {
    
    public func asyncCall(apiRequest: TFRequest,requiredResponce: ResponseType, WithDelegate delegate:UIViewController? = nil, completion:@escaping tfServiceCompletionerHandler)
    {
        guard let _ = apiRequest.url else
        {
            completion(TFResponse(error: ErrorModel(errorTitle: "URL Error", errorDescp: "Url is not correct", error: nil)))
            return
        }
        
        guard apiRequest.valedateQueryStringInRequest() else {
            assertionFailure("access_token set in TFRequest property quertyString instead of url path")
            return
        }
        
        var headers: HTTPHeaders? = apiRequest.header
        if headers == nil {
            headers = authHeader  // setting header if request dont have it
        }
        
        guard let _ = headers else {
            completion(TFResponse(error: ErrorModel(errorTitle: "Header Error", errorDescp: "Header not set", error: nil)))
            print("header is nil")
            return
        }
        
        callAPI(apiRequest, requiredResponce: requiredResponce, header: headers, completion)
    }
    
    private func callAPI(_ apiRequest: TFRequest,requiredResponce: ResponseType, header: HTTPHeaders? = nil,_ completion: @escaping tfServiceCompletionerHandler) {
        
        requestExecute(apiRequest: apiRequest, requiredResponce: requiredResponce, encoding: JSONEncoding.default, header: header) { (jsonResp, dataResp, stringResp) in
            
            if requiredResponce == .json {
                self.handleJOSN(response: jsonResp!, completion)
            }
            else if requiredResponce == .data {
                self.handleData(response: dataResp!, completion)
            }
            else if requiredResponce == .plain {
                self.handleString(response: stringResp!, completion)
            }
        }
    }
}

extension TFNetworkManager {
    
    //MARK:- Handle JSON Response
    private func handleJOSN(response: DataResponse<Any,AFError>, _ completion: @escaping tfServiceCompletionerHandler) {
        
        let httpResponse = response.response
        
        var apiResponse = TFResponse()
        
        apiResponse.responceType = ResponseType.json
        apiResponse.statusCode = httpResponse?.statusCode
        apiResponse.data = response.value
        apiResponse.request = response.request
        
        switch response.result
        {
        case .success:
            
            if httpResponse?.statusCode == 200
            {
                if response.error != nil {
                    apiResponse.error = ErrorModel(errorTitle: "", errorDescp: response.error?.localizedDescription ?? "", error: nil)
                    DispatchQueue.main.async {
                        completion(apiResponse)
                    }
                    return
                }
                
                if let res = response.value
                {
                    let jsonVal = JSON(res)
                    if let error = handleGraphQlInternalError(jsonVal) {
                        apiResponse.error = error
                        DispatchQueue.main.async {
                            completion(apiResponse)
                        }
                        return
                    }
                    
                    
                    let rawJson = jsonVal.rawValue
                    
                    if ((rawJson as? [String:Any]) != nil) || ((rawJson as? [Any]) != nil) 
                    {
                        DispatchQueue.main.async {
                            completion(apiResponse)
                        }
                    }
                    else
                    {
                        apiResponse.error = ErrorModel(errorTitle: "Json Parsing Error", errorDescp: "Invalid json body", error: nil, errorCode:0)
                        DispatchQueue.main.async {
                            completion(apiResponse)
                        }
                    }
                }
                else
                {
                    apiResponse.error = ErrorModel(errorTitle: "Server Error", errorDescp: "Got Response Nil", error: nil)
                    DispatchQueue.main.async {
                        completion(apiResponse)
                    }
                }
            }
            else
            {
                apiResponse.error = ErrorModel(errorTitle: "", errorDescp: response.error?.localizedDescription ?? "cannot get the AF Error log", error: nil)
                self.errorHandling(response: apiResponse) { (tfResp) in
                    DispatchQueue.main.async {
                        completion(tfResp)
                    }
                }
            }
            
            
            
            break;
        case .failure(let error):
            
            apiResponse.error = ErrorModel(errorTitle: "", errorDescp: error.localizedDescription, error: nil)
            self.errorHandling(response: apiResponse) { (tfResp) in
                DispatchQueue.main.async {
                    completion(tfResp)
                }
            }
            
        }
    }
    
    //MARK:- Handle Data Response
    private func handleData(response: DataResponse<Data,AFError>, _ completion: @escaping tfServiceCompletionerHandler) {
        
        let httpResponse = response.response
        
        var apiResponse = TFResponse()
        apiResponse.responceType = ResponseType.data
        apiResponse.statusCode = httpResponse?.statusCode
        apiResponse.data = response.value
        apiResponse.request = response.request
        
        switch response.result
        {
        case .success:
            
            if httpResponse?.statusCode == 200
            {
                if response.error != nil {
                    apiResponse.error = ErrorModel(errorTitle: "", errorDescp: response.error?.localizedDescription ?? "", error: nil)
                    DispatchQueue.main.async {
                        completion(apiResponse)
                    }
                    return
                }
                
                if let res = response.value
                {
                    let jsonVal = JSON(res)
                    
                    if let error = handleGraphQlInternalError(jsonVal) {
                        apiResponse.error = error
                        DispatchQueue.main.async {
                            completion(apiResponse)
                        }
                        return
                    }
                    
                    DispatchQueue.main.async {
                        completion(apiResponse)
                    }
                    
                }
                else
                {
                    apiResponse.error = ErrorModel(errorTitle: "Server Error", errorDescp: "Got Response Nil", error: nil)
                    DispatchQueue.main.async {
                        completion(apiResponse)
                    }
                }
            }
            else
            {
                apiResponse.error = ErrorModel(errorTitle: "", errorDescp: response.error?.localizedDescription ?? "cannot get the AF Error log", error: nil)
                self.errorHandling(response: apiResponse) { (tfResp) in
                    DispatchQueue.main.async {
                        completion(tfResp)
                    }
                }
            }
            
            
            
            break;
        case .failure(let error):
            
            apiResponse.error = ErrorModel(errorTitle: "", errorDescp: error.localizedDescription, error: nil)
            self.errorHandling(response: apiResponse) { (tfResp) in
                DispatchQueue.main.async {
                    completion(tfResp)
                }
            }
            
        }
    }
    
    //MARK:- Handle String Response
    private func handleString(response: DataResponse<String,AFError>, _ completion: @escaping tfServiceCompletionerHandler) {
        
        let httpResponse = response.response
        
        var apiResponse = TFResponse()
        apiResponse.responceType = ResponseType.data
        apiResponse.statusCode = httpResponse?.statusCode
        apiResponse.data = response.value
        apiResponse.request = response.request
        
        switch response.result
        {
        case .success:
            
            if httpResponse?.statusCode == 200
            {
                if response.error != nil {
                    apiResponse.error = ErrorModel(errorTitle: "", errorDescp: response.error?.localizedDescription ?? "", error: nil)
                    DispatchQueue.main.async {
                        completion(apiResponse)
                    }
                    return
                }
                
                
                if let _ = response.value
                {
                    DispatchQueue.main.async {
                        completion(apiResponse)
                    }
                    
                }
                else
                {
                    apiResponse.error = ErrorModel(errorTitle: "Server Error", errorDescp: "Got Response Nil", error: nil)
                    DispatchQueue.main.async {
                        completion(apiResponse)
                    }
                }
            }
            else
            {
                apiResponse.error = ErrorModel(errorTitle: "", errorDescp: response.error?.localizedDescription ?? "cannot get the AF Error log", error: nil)
                self.errorHandling(response: apiResponse) { (tfResp) in
                    DispatchQueue.main.async {
                        completion(tfResp)
                    }
                }
            }
            
            
            
            break;
        case .failure(let error):
            
            apiResponse.error = ErrorModel(errorTitle: "", errorDescp: error.localizedDescription, error: nil)
            self.errorHandling(response: apiResponse) { (tfResp) in
                DispatchQueue.main.async {
                    completion(tfResp)
                }
            }
            
        }
    }
}

//MARK:- Error Handling
extension TFNetworkManager {
    
    fileprivate func handleGraphQlInternalError(_ jsonVal: JSON) -> ErrorModel? {
        
        guard let parseJson = jsonVal.rawValue as? [String:Any] else {
            return nil
        }
        
        let graphQLError = parseJson["errors"] as? [[String:Any]]
        if  let _ = graphQLError
        {
            if (graphQLError!.count > 0)
            {
                print(graphQLError![0])
                let msgDic = graphQLError![0];
                
                if let msg = msgDic["message"] as? String
                {
                    return ErrorModel(errorTitle: "GraphQL Error", errorDescp: msg, error: nil, errorCode: 0)
                }
                else
                {
                    return ErrorModel(errorTitle: "GraphQL Error", errorDescp: "Unknown Error", error: nil)
                }
            }
        }
        
        return nil
    }
    
    fileprivate func errorHandling(response: TFResponse,completionHandler:tfServiceCompletionerHandler)
    {
        if  let statusCode = response.statusCode
        {
            switch(statusCode)
            {
            case AuthenticationFailedStatusCode:
                self.setErrorForAuthFailed(response: response, completionHandler: completionHandler)
                
            case 403:
                var rsp = response
                rsp.error = ErrorModel(errorTitle: "Meera", errorDescp: "Access is Denied", error: nil)
                completionHandler(rsp)
                
            case 400:
                completionHandler(response)
                
            default:
                completionHandler(response)
            }
        }
        else {
            completionHandler(response)
        }
    }
    
    //MARK:- Token Expire Error
    fileprivate func setErrorForAuthFailed(response: TFResponse,completionHandler:tfServiceCompletionerHandler)
    {
        if response.statusCode == AuthenticationFailedStatusCode
        {
            var rsp = response
            print("QaisNetworkManger: responseCheckForRefreshToken(response.... authentication failure on graphql api")
            rsp.error = ErrorModel(errorTitle: "Meera", errorDescp: "Authentication Failed", error: nil)
            completionHandler(rsp)
        }
        else {
            completionHandler(response)
        }
    }
}

extension TFNetworkManager {
    
    func renewAuthToken(_ completionHandler: @escaping (Bool) -> Void) {
        let refreshToken = token?.refresh_token
        guard NetworkHelper.sharedInstance.isValidText(refreshToken) == true else {
            
            self.delegate?.didFailedRenewToken()
            
            completionHandler(false)
            return
        }
        
        TFAuth.renewToken(refreshToken: refreshToken!) { [weak self] (tokenModel) in
            
            guard let self = self else {return}
            if let model = tokenModel {
                
                self.token = model
                
                self.delegate?.didAccessTokenChanged(token: self.token!)
                
                completionHandler(true)
            }
            else {
                self.delegate?.didFailedRenewToken()
                completionHandler(false)
            }
        }
    }
}

