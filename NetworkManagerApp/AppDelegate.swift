//
//  AppDelegate.swift
//  NetworkManagerApp
//
//  Created by Waqas Rasheed on 11/04/2021.
//

import UIKit
import TFNetwork

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        Config.setUrlConfiguration(server: .target)
        
        return true
    }



}

