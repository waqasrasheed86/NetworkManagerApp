//
//  Configurator.swift
//  BloomingBuyer
//
//  Created by Waqas Rasheed on 22/03/2021.
//

import Foundation

//let config = Config.sharedInstance

let restApiVersion = "/api/v1"

struct Config {
    
    //static let sharedInstance = Config()
    
    private static var serverEnum: ServerEnum = .dev
    private static let scheme = "https://"
    
    static var domainName = "dev.meeraspace.com";
    static var serverURL = ""
    static var ssoURL = ""
    static var zeenaUrl = ""
    static var enmaUrl = ""
    static var restBaseUrl = ""
    static var baseXeenaPayUrl = ""
    
    static private var reporterUrl_ = "" // use reporterUrl instead of reporterUrl_
    
    static var csrUrl = ""
    
    static func setUrlConfiguration(server: ServerEnum) {
        
        self.serverEnum = server
        
        switch self.serverEnum {
        
        case .dev:
            
            self.domainName = "dev.meeraspace.com";
            self.ssoURL = "\(scheme)sso.\(domainName)"
            
        case .demo_aws:
            
            self.domainName = "demo-aws.meeraspace.com";
            self.ssoURL = "\(scheme)sso.\(domainName)"
            
        case .om_xeena:
            
            self.domainName = "om.xeena.com";
            self.ssoURL = "\(scheme)sso.\(domainName)"
            
        case .xeena:
            self.domainName = "xeena.com";
        case .target:
            self.domainName = "target.fluxble.com"
            self.ssoURL = "\(scheme)sso.fluxble.com"
        }
        
        self.serverURL = "\(scheme)\(domainName)"
        
        //self.ssoURL = "\(scheme)sso.\(domainName)"
        
        self.zeenaUrl = "\(scheme)zeena.\(domainName)"
        
        self.restBaseUrl = "\(scheme)api.\(domainName)"    // ( wf-be)  (restBaseUrlNoteCenter= /xeena-corporate-be")
        self.baseXeenaPayUrl = "\(scheme)pay.\(domainName)"
        
        self.enmaUrl = "\(scheme)reach.\(domainName)"
        self.csrUrl = "\(scheme)csr.\(domainName)" //"/meera-csr/api"
        
        if serverEnum == .dev {
            self.reporterUrl_ = "\(scheme)reporter.\(domainName)" // /api"
        }
        else {
            self.reporterUrl_ = "\(scheme)api.\(domainName)"  // /rep/api
        }
    }
    
    static var isDev: Bool {
        return self.serverEnum == .dev
    }
    
    enum ServerEnum {
        case dev
        case demo_aws
        case om_xeena
        case xeena
        case target
    }
}

extension Config {
    
    static var zeenaGraphUrL: String {
        return zeenaUrl + "/graphql"
    }
    
    static  var enmaGraphUrL: String {
        return enmaUrl + "/graphql"
    }
    
    static var imgbaseUrl: String {
        return restBaseUrl + "/fm"
    }
    
    static var reporterUrl: String {
        if serverEnum == .dev {
            return self.reporterUrl_ + "/api"
        }
        else {
            return self.reporterUrl_ + "/rep/api"
        }
    }
    
    static var imgbaseUploadUrl: String {
        return "\(imgbaseUrl)/upload"
    }
    
    static var imgbaseDownloadUrl: String {
        return "\(imgbaseUrl)/download"
    }
    
    static var webFileUrl: String {
        return imgbaseUrl
    }
}

//var domainName = "dev.meeraspace.com"
//var domainName = "demo-aws.meeraspace.com"
//var domainName = "om.xeena.com"
//var domainName = "xeena.com"


