//
//  ViewController.swift
//  NetworkManagerApp
//
//  Created by Waqas Rasheed on 11/04/2021.
//

import UIKit
import SwiftyJSON
import TFNetwork
import OAuthSwift

var oauthswift = OAuth2Swift(
    consumerKey: "mobile-app", //meera-dx "mobile-app"        // [1] Enter google app settings
    consumerSecret:"ZXhhbXBsZS1hcHAtc2VjcmV1",        // No secret required
    authorizeUrl: Config.ssoURL + "/auth",
    accessTokenUrl: Config.ssoURL + "/token",
    responseType: "code"
)

class ViewController: UIViewController, TFNetworkManagerDelegate {
    
    
    
    var token: TFAuth.TFTokenModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //let p = TestJSON()
        
        
        TFAuth.setOauthswift(oauthswift: oauthswift)
        
        TFAuth.authenticate(with: "waqas.rasheed@targetofs.com", password: "mmmmmmmmm") { (model, error) in
            self.token = model
            model?.printValues()
            
            TFNetwork.setToken(authToken: model!)
            //TFNetwork.delegate = self
            
        }
        
        
        
    }
    
    func didFailedRenewToken() {
        print("didFailedRenewToken")
    }
    
    func didAccessTokenChanged(token: TFAuth.TFTokenModel) {
        print("didAccessTokenChanged")
    }
    
    func stringify(json: Any, prettyPrinted: Bool = false) -> String {
        var options: JSONSerialization.WritingOptions = []
        if prettyPrinted {
          options = JSONSerialization.WritingOptions.prettyPrinted
        }

        do {
          let data = try JSONSerialization.data(withJSONObject: json, options: options)
          if let string = String(data: data, encoding: String.Encoding.utf8) {
            return string
          }
        } catch {
          print(error)
        }

        return ""
    }
    
}


