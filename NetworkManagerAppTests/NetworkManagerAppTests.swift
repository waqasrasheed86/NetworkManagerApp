//
//  NetworkManagerAppTests.swift
//  NetworkManagerAppTests
//
//  Created by Waqas Rasheed on 11/04/2021.
//

import XCTest
import SwiftyJSON
import TFNetwork
import OAuthSwift
@testable import NetworkManagerApp

class NetworkManagerAppTests: XCTestCase {
    
    var token: TFAuth.TFTokenModel!
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}

extension NetworkManagerAppTests {
    
    func test1() {
        TFAuth.setOauthswift(oauthswift: oauthswift)
        
        let expctAuth =  expectation(description: "authenticate")
        TFAuth.authenticate(with: "waqas.rasheed@targetofs.com", password: "mmmmmmmmm") { (model, error) in
            
            expctAuth.fulfill()
            
            self.token = model
            //model?.printValues()
            
            TFNetwork.setToken(authToken: model!)
            TFNetwork.delegate = self
            
        }
        
        waitForExpectations(timeout: 10) { (error) in
            print(error?.localizedDescription ?? "")
        }
        
        var parameter = [String:Any]();
        parameter["query"] = Q_UserInfoV2
        
        let baseUrl = "https://" + "target.fluxble.com" + "/graphql"
        
        let req = TFRequest(baseUrl: baseUrl, method: .post, bodyPerameter: parameter)
        let expectUserinfo =  expectation(description: "expectUserinfo")
        TFNetwork.asyncCall(apiRequest: req, requiredResponce: .plain) { (responce) in
            
            if responce.error == nil {
                print(responce.data!)
                expectUserinfo.fulfill()
            }
        }
        waitForExpectations(timeout: 10) { (error) in
            print(error?.localizedDescription ?? "")
        }
        
        
        
        
        
    }
}

extension NetworkManagerAppTests: TFNetworkManagerDelegate {
    func didFailedRenewToken() {
        print("Faied to renew")
    }

    func didAccessTokenChanged(token: TFAuth.TFTokenModel) {
        
        print("didAccessTokenChanged")
    }


}

let Q_UserInfoV2 = """
{
    mev2 {
        roles
        primaryRole
        user {
            id
            subject
            profile {
                userID
                fullName
                title
                pictureURL
                email
                nationality
                dateOfBirth
                gender
                streetAddress
                photo
                phoneMobile
                displayName
            }
        }
    }
}
"""
