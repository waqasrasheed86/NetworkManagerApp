
Pod::Spec.new do |spec|  
  spec.name         = "TFNetwork"
  spec.version      = "1.0"
  spec.summary      = "A short description of TFNetwork"

  spec.description  = <<-DESC
                    TFNetwork manage the expiry of the token
                    DESC

  spec.homepage     = "https://gitlab.com/waqasrasheed86/NetworkManagerApp"
  spec.license      = { :type => "MIT", :file => "LICENSE" }
  spec.author       = { "Waqas Rasheed" => "raoowaqas@gmail.com" }
  spec.platform     = :ios, "11.0"

  spec.source       = { :git => "https://gitlab.com/waqasrasheed86/NetworkManagerApp.git", :tag => "v1.0.2"}

 spec.swift_version = "5.0"
 spec.source_files = "TFNetwork/**/*.{swift}"
  


  #spec.exclude_files = "Classes/Exclude"
  # spec.public_header_files = "Classes/**/*.h"
  # spec.resource  = "icon.png"
  # spec.resources = "Resources/*.png"

 spec.static_framework = true
 spec.framework    = "UIKit"
 spec.dependency 'Alamofire', '~> 5.2'
 spec.dependency 'SwiftyJSON'

 

 spec.dependency 'OAuthSwift' #, :git => 'https://github.com/OAuthSwift/OAuthSwift.git'





end
